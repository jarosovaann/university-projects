import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.measure.ResultsTable;
import ij.plugin.filter.Analyzer;
import ij.process.ShortProcessor;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.lang.Math;


class Pixel
{
	/** Pixel coordinates. */
	int x, y;

	/** Constructor. */
	Pixel(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}

/** This class encapsulates the parameters of a bounding box. */
class BoundingBox
{
	/** The coordinates of the bounding box origin (i.e., its top-left corner). */
	Pixel origin;
	/** The width of the bounding box. */
	int width;
	/** The height of the bounding box. */
	int height;

	/** Constructor. */
	BoundingBox(Pixel origin)
	{	
		this.origin = origin;
		width = 1;
		height = 1;
	}

	/** Update the bounding box parameters based on the coordinates of a newly added pixel. */
	void update(Pixel pixel)
	{
		if (pixel.x < origin.x)
		{
			width += origin.x - pixel.x;
			origin.x = pixel.x;
		}
		else
		{
			width = Math.max(width, pixel.x - origin.x + 1);
		}

		if (pixel.y < origin.y)
		{
			height += origin.x - pixel.x;
			origin.y = pixel.y;
		}
		else
		{
			height = Math.max(height, pixel.y - origin.y + 1);
		}
	}
}

class Region
{
	/** The bounding box of the region. */
	BoundingBox bbox;
	/** The region label. */
	int label;
	/** The region size. */
	int size;
	/** X-coordinate of the region centroid. */
	double cX;
	/** Y-coordinate of the region centroid. */
	double cY;		
	/** The region eccentricity. */
	double ecc;
	/** The perimeter of obejct. */
	int perimeter;
	/** The circularity of object */
	float circ;
	/** Constructor. */
	Region(Pixel origin, int label)
	{
		this.bbox = new BoundingBox(origin);
		this.label = label;
		this.size = 0;
		this.cX = 0.0;
		this.cY = 0.0;
		this.ecc = -1.0;
		this.perimeter = 0;
		this.circ = 0f;
	}
} 

/** This plugin segments and classifies objects according to their shape in a given Candy Crush board. */
public class ObjectFinder implements PlugInFilter
{
	final static int BACKGROUND_LABEL = 255; // alias for the background label of a binary image
	final static int TMP_LABEL = 128;
	final static int UNLABELED = 120;
	
	
	public int setup(String arg, ImagePlus im) 
	{
		if (im != null)
		{			
			// this plugin accepts RGB images only
			return DOES_RGB + NO_CHANGES;
		}
		else
		{
			// no image is open
			IJ.noImage();
			return -1;
		}
	}

	public void run(ImageProcessor ip) 
	{	

		//ImagePlus ip = IJ.getImage();
		// extract the blue channel from the input RGB image
		ByteProcessor bip = ((ColorProcessor) ip).getChannel(3, null);
		
		double sigma = 1.5;		
		bip.blurGaussian(sigma);

		/* part of candies are dark and the other ones are light (bsc we use blue channel)
		so we threshold image, we need two thresholds */

		int thresh = findThresh(bip, 0);
		int thresh1 = findThresh(bip, thresh);
		int pixelCount = bip.getPixelCount();

		for (int x = 0; x < pixelCount; ++x)
		{
			if (bip.get(x) < thresh || bip.get(x) > thresh1)
			{
				bip.set(x, 0);
			}
			else
			{
				bip.set(x, 255);
			}
		}


		/* following code performs hole filling because we need objects without holes*/

		int w = bip.getWidth();
		int h = bip.getHeight();

		for (int x = 0; x < w; ++x)
		{
			floodFill(bip, x, 0, BACKGROUND_LABEL, TMP_LABEL);
			floodFill(bip, x, h - 1, BACKGROUND_LABEL, TMP_LABEL);
		}

		for (int y = 1; y < h; ++y)
		{
			floodFill(bip, 0, y, BACKGROUND_LABEL, TMP_LABEL);
			floodFill(bip, w - 1, y, BACKGROUND_LABEL, TMP_LABEL);

		}

		for (int i = 0; i < pixelCount; ++i)
		{
			if (bip.get(i) == TMP_LABEL)
			{
				bip.set(i, 0);
			}
			else
			{
				bip.set(i, 255);
			}		
		}

		/* here I am finding edges of the objects, I am using erossion and then I check the differences of original image
		and eroded image, the difference of those two gives me inside border */

		ImageProcessor mask = bip.duplicate();
		binaryEroDilWithDiskSE(mask, 2, 0);

		int num = w * h;
		for (int i = 0; i < num; ++i)
		{
			int actualBip = bip.get(i);
			if (actualBip == 255)
			{
				bip.set(i, UNLABELED);
			}
			if (actualBip != mask.get(i))
			{
				mask.set(i, 255);
			}
			else{
				mask.set(i,0);
			}
		}


		ArrayList<Region> rList = new ArrayList<Region>();
		int label = 1;	

		// go through 'bip' and label individual 
		// 8-connected foreground regions using a breadth-first flood filling routine
		for (int y = 0; y < h; ++y)
		{
			for (int x = 0; x < w; ++x)
			{
				if (bip.get(x, y) == UNLABELED)
				{
					rList.add(floodFill(bip, x, y, label));
					++label;
				}
			}
		}

		measure(bip, mask, rList);


		// create an output RGB image as a copy of the input RGB image
		ImageProcessor out = ip.duplicate();

		// each group of objects is represented by different color

		/* to make the outline thick */
		binaryEroDilWithDiskSE(mask, 2, TMP_LABEL);
		
		classifyObjects(bip, mask, out, rList);

		// NO CHANGES NEEDED AFTER THIS LINE

		// display the final labeled image out
		ImagePlus outImg = new ImagePlus("My object classification:", out);
		outImg.show();

	}

	/* this function measures objects
	the atributes are needed for further classification */
	private void measure(ImageProcessor bip, ImageProcessor mask, ArrayList<Region> rList)
	{

		int h = bip.getHeight();
		int w = bip.getWidth();


		Region reg;
		for (int y = 0; y < h; ++y)
		{
			for (int x = 0; x < w; ++x)
			{
				int label = bip.get(x, y);
				if (label > 0 && label <= rList.size())
				{
					
					reg = rList.get(label - 1);
					if (reg.perimeter == 0)
					{
						reg.perimeter = floodFill(mask, x, y, 255, TMP_LABEL);
					}
					reg.size++;
					reg.cX += x;
					reg.cY += y;

				}
			}
		}

		double mi02;		
		double mi20;
		double mi11;

		double sqRoot;
		double mi2sum;
		for (int i = 0; i < rList.size(); ++i)
		{
			reg = rList.get(i);
			reg.cX /= reg.size;
			reg.cY /= reg.size;
			reg.circ = 4f * (float) Math.PI * ((float) reg.size / (reg.perimeter * reg.perimeter)) ;
			mi02 = centralMoment(bip, reg, 0, 2);			
			mi20 = centralMoment(bip, reg, 2, 0);
			mi11 = centralMoment(bip, reg, 1, 1);


			sqRoot = Math.sqrt((mi20 - mi02) * (mi20 - mi02) + 4 * (mi11 * mi11));
			mi2sum = mi20 + mi02;
			reg.ecc = (mi2sum  + sqRoot) / (mi2sum - sqRoot);

		} 
	}


	/** Compute the central moment of order 'p' and 'q' for the given binary region 'reg' in the image 'bip'. */  
	private double centralMoment(ImageProcessor bip, Region reg, int p, int q)
	{
		double centralM = 0;
		for (int y = reg.bbox.origin.y; y <reg.bbox.origin.y + reg.bbox.height; ++y)
		{
			for (int x = reg.bbox.origin.x; x < reg.bbox.origin.x + reg.bbox.width; ++x)
			{
				if (bip.get(x, y) == reg.label)
				{
					centralM += Math.pow((x - reg.cX), p) * Math.pow((y - reg.cY), q);
				}
			}
		}
		return centralM;
	}

	/** Classify regions in the given list of regions 'rList' into three categories depending on their eccentricity and circularity*/
	private void classifyObjects(ImageProcessor cip, ImageProcessor mask, ImageProcessor out, ArrayList<Region> rList)
	{
		float[] clasArray = new float[rList.size()];

		for (int i = 0; i < rList.size(); ++i)
		{
			double ecc = rList.get(i).ecc;
			float circ = rList.get(i).circ;
			if (ecc >= 3.2) // elongated tilted objects based on eccentrity and other (roughly circular)
			{
				clasArray[i] = 0x00ffff;
			}
			else if (circ < 0.29) // square based on small circularity value 
			{
				clasArray[i] = 0xffff00;			
			}
			else // and other (roughly circular) objects
			{
				clasArray[i] = 0xff00ff;			
			}
			
		}

		// draws the colored inner contour for each candy to the 'out' image based on classification
		for (int i = 0; i < cip.getPixelCount(); ++i)
		{
			int labelCIP = cip.get(i);
			int labelMask = mask.get(i);
			if (labelMask == TMP_LABEL && labelCIP > 0 && labelCIP <= rList.size())
			{
				if (rList.get(labelCIP - 1).size > 1000)
					out.setf(i, clasArray[labelCIP - 1]);
			}

		}
	}

	private int floodFill(ImageProcessor ip, int x, int y, int criticalIntenz, int label)
	{
		Queue<Pixel> q = new ArrayDeque<Pixel>();
		int perimeter = 0;
		int w = ip.getWidth();
		int h = ip.getHeight();

		q.add(new Pixel(x, y));
		while (! q.isEmpty())
		{
			Pixel val = q.remove();
			if ((val.x >= 0 && val.x < w) && ( val.y >= 0 && val.y < h) && ip.get(val.x, val.y) == criticalIntenz)
			{
				ip.set(val.x, val.y, label);
				q.add(new Pixel(val.x - 1, val.y));
				q.add(new Pixel(val.x + 1, val.y));
				q.add(new Pixel(val.x, val.y - 1));
				q.add(new Pixel(val.x, val.y  + 1));
				q.add(new Pixel(val.x - 1, val.y - 1));
				q.add(new Pixel(val.x + 1, val.y + 1));
				q.add(new Pixel(val.x + 1, val.y - 1));
				q.add(new Pixel(val.x - 1, val.y  + 1));
				++perimeter;
			}
		}
		return perimeter;
	}

	private Region floodFill(ImageProcessor bip, int x, int y, int label)
	{
		Region reg = new Region(new Pixel(x, y), label);
		
		int w = bip.getWidth();
		int h = bip.getHeight();
		
		Queue<Pixel> q = new ArrayDeque<Pixel>();

		q.add(new Pixel(x, y));
		
		while (!q.isEmpty())
		{
			Pixel n = q.remove();
			if (n.x >= 0 && n.x < w && n.y >= 0 && n.y < h && bip.get(n.x, n.y) == UNLABELED)
			{
				reg.bbox.update(n);
				bip.set(n.x, n.y, label);
				q.add(new Pixel(n.x - 1, n.y));
				q.add(new Pixel(n.x + 1, n.y));
				q.add(new Pixel(n.x, n.y - 1));
				q.add(new Pixel(n.x, n.y + 1));
				
				q.add(new Pixel(n.x - 1, n.y - 1));
				q.add(new Pixel(n.x + 1, n.y + 1));
				q.add(new Pixel(n.x + 1, n.y - 1));
				q.add(new Pixel(n.x - 1, n.y  + 1));
			}
		}
		return reg;
	}

	/** Applies the erosion (critical_value equals to 0) or dilation (critical_value equals to 255) */
	private void binaryEroDilWithDiskSE(ImageProcessor ip, int radius, int critical_value)
	{
		int w = ip.getWidth();
		int h = ip.getHeight();

		ImageProcessor copy = ip.duplicate();
		int pixelValue;

		for (int y = 0; y < h; ++y)
		{
			for (int x = 0; x < w; ++x)
			{
				int v1 = -radius;
				int v2 = radius;
				int u1 = -radius;
				int u2 = radius;
				
				if (y - radius < 0)
				{ 
					v1 = - y;
				}
				if (y + radius >= h) {
					v2 = h - y - 1;
				}
				if (x - radius < 0) {
					u1 = - x;
				}
				if (x + radius >= w) {
					u2 = w - x - 1;
				}
				
				pixelValue = -1;
				for (int j = v1; j <= v2 && pixelValue != critical_value; ++j)
				{
					for (int i = u1; i <= u2 && pixelValue != critical_value; ++i)
					{
						if ( i*i + j*j <= radius * radius)
						{
							pixelValue = copy.get(x + i, y + j);
						}
					}
				}

	 			if (pixelValue == critical_value)
				{
					ip.set(x,y,critical_value);
				} 
			}
		}
	}

	/* finds a suitable threshold for the image based on the gradient information and limited by critical_value*/
	private int findThresh(ImageProcessor ip, int critical_value){

 		int w = ip.getWidth();
		int h = ip.getHeight();


		double gradMagnitude;
		double numx;
		double numy;
		
		
		double sumGradMagnitude = 0;
		double prodGradMagnitude = 0;


		for (int y = 1; y < h - 1; ++y)
		{
			for (int x = 1; x < w - 1; ++x)
			{
				// as long as I know that first threshold is the 'lower' one (there are more 'dark candies')
				// I can use this condition, if there would be more or equal 'light' candies 
				// I would have to consider a little bit different condition eg. not '>' but '<'
				if (ip.get(x + 1, y) > critical_value
				&& ip.get(x + 1, y) > critical_value  && ip.get(x - 1, y) > critical_value
				&& ip.get(x, y + 1) > critical_value && ip.get(x, y - 1) > critical_value)
				{
					numx = (ip.get(x + 1, y) - ip.get(x - 1, y)) * 0.5;
					numy = (ip.get(x, y + 1) - ip.get(x, y - 1)) * 0.5;
					gradMagnitude = (Math.sqrt(numx * numx + numy * numy));
	
					prodGradMagnitude += ip.get(x, y) * gradMagnitude;
					sumGradMagnitude += gradMagnitude; 
	
				}
			}
		}
		return ((int) (prodGradMagnitude / sumGradMagnitude + 0.5));
	}
}