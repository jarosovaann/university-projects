# University projects


## Overview

In this repository are four folders **machine-learning-chords**, **algorithms**, **image-processing** and **oop**.

## Machine learning
This folder contains a team project focusing on chord classification. The input data are audio files in `.wav` format. Project consists of data visualization, preprocessing, choosing and evaluating models.

The models cosidered in this work are:
- Naive bayes as a baseline
- KNN
- Decision Tree Classifier
- Random Forest Classifier
- SVM

The project is written in IPython using libraries such as Pandas, Numpy, Scikit-learn etc. IPython focuses on interactive Python and in this case is used in Jupyter notebook -- an interactive computational environment (https://docs.jupyter.org/en/latest/start/index.html).

## Algorithms
Folder algorithms focuses on data structures and algorithms with it's time-complexity. Each file has a short description inside. 
```
    Description: An implementation of Breadth First Search algorithm to find reachable part of directed graph
    
    Time complexity: O(|Vertices| + |Edges|)
    Extra space complexity: O(|Vertices|)
    
    Input: Start vertex

    Output: A tuple[number of reachable vertices, number of reachable edges]
```
## Image-processing

This directory is under a continuos development. It contains plugin performing automatic object recognition. Folder also contains input images and expected output with labeled clusters. Note that instaled ImageJ environment is needed to perform the object recognition. For more information read https://imagej.nih.gov/ij/docs/pdfs/tutorial12.pdf


## OOP
Folder _oop_ focuses on Object-oriented programming in C#, Java, Python.

### C# projects
_Object-hierarchy-I_ and _object-hierachy-II_ works with interface/heredity/virtual and overriding methods etc.. 

### Java projects
_App-text-operations_ is an advanced project focused on working with text. Folder app-text-operations has it's own README inside.

### Python projects
_Folder-structure_ works with directories and files hierarchy. This illustrates OOP in Python.
