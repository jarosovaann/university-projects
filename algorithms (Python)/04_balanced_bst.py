from typing import Optional, TextIO, List

"""
    BSTree is a binary tree where each node stores it's subtree size.
"""

class Node:
    def __init__(self, key: int,
                 left: Optional['Node'] = None,
                 right: Optional['Node'] = None,
                 parent: Optional['Node'] = None):
        self.key = key
        self.left = left
        self.right = right
        self.parent = parent
        self.size = 1


class BSTree:
    def __init__(self, root: Optional[Node] = None):
        self.root = root


# Part 1.
"""
    Description: An algorithm searching through BSTree and checking if size attribute is correct
    
    Time complexity: O(Nodes)
    
    Input: BSTree
    
    Ouput: True is size attributes are correct in each node, False otherwise
"""


def check_size(tree: BSTree) -> bool:
    if tree.root is None:
        return True
    nd: Node = tree.root
    return check_size_rec(nd)


def check_size_rec(node: Node) -> bool:
    if node.left is None and node.right is None:
        return node.size == 1
    if node.left is None:
        if node.size != (1 + node.right.size):
            return False
        return check_size_rec(node.right)
    elif node.right is None:
        if node.size != (1 + node.left.size):
            return False
        return check_size_rec(node.left)
    else:
        if node.size != node.left.size + node.right.size + 1:
            return False
        return check_size_rec(node.left) and check_size_rec(node.right)


"""
    Description: Function checks is node is 3/5 balanced. 
        (Node is 3/5 balanced if both of it's sons subtree sizes are at most 3/5 times of a node size)

    Time complexity: O(Nodes)

    Input: BSTree (with correct size attributes)
    
    Output: True if all nodes of BSTree are 3/5 balanced, False otherwise
"""


def check_3_5_balanced(tree: BSTree) -> bool:
    if tree.root is None:
        return True
    nd: Node = tree.root
    return check_3_5_rec(nd)


def check_3_5_rec(node: Node) -> bool:
    if node.right is None and node.left is None:
        return True
    if node.right is None:
        if node.left.size > node.size * 3/5:
            return False
        return check_3_5_rec(node.left)
    elif node.left is None:
        if node.right.size > node.size * 3/5:
            return False
        return check_3_5_rec(node.right)
    else:
        if node.right.size > node.size * 3/5 \
                or node.left.size > node.size * 3/5:
            return False
        return check_3_5_rec(node.left) and check_3_5_rec(node.right)


def is_in_tree(node: Optional[Node], key: int) -> bool:
    if node is None:
        return False
    if node.key == key:
        return True
    if key < node.key:
        return is_in_tree(node.left, key)
    return is_in_tree(node.right, key)


# Part 2.
"""
    Description: Insert to a BSTree, if node with given key already in BSTree exist, nothing happens,
                if new node ruins 3/5 balance function returns a link to problem node
    
    Time complexity: O(log nodes)
    
    Input: Correct BSTree, integer
    
    Output: None if key already exist in BSTree or if insert can be done without ruining 3/5 balance,
            link to problem node if 3/5 balance is ruined
"""


def insert(tree: BSTree, key: int) -> Optional[Node]:
    if is_in_tree(tree.root, key):
        return None
    ret: Optional[Node] = None
    node = tree.root
    if node is None:
        tree.root = Node(key)
    while node is not None:
        node.size += 1
        if key < node.key:
            child = node.left
            if child is None:
                childer(node, key, True)
                return ret
        else:
            child = node.right
            if child is None:
                childer(node, key, False)
                return ret
        if node.size * (3/5) < child.size + 1:
            if ret is None:
                ret = node
        node = child
    return ret


def childer(node: Node, key: int,
            left: bool) -> None:
    child = Node(key)
    if left:
        node.left = child
    else:
        node.right = child
    child.parent = node


# Part 3.
"""
    Description: Function rebalances from given node down to leaves to be 1/2 balanced 
    (Node is 1/2 balanced if both of it's sons subtree sizes are at most 1/2 times of a node size)
    
    Time complexity: O(node.size)
    
    Input: BSTree, node to be rebalanced
    
    Ouput: None (Given tree is rebalanced down from given node (incuding given node))
"""


def rebalance(tree: BSTree, node: Node) -> None:
    parent = node.parent
    lst: List[Node] = []
    inorder(node, lst)
    for i in lst:
        i.left = None
        i.right = None
    bin_search(lst, 0, len(lst)-1, None)
    if parent is None:
        tree.root = lst[(len(lst)-1)//2]
    else:
        lst[(len(lst)-1)//2].parent = parent
        if lst[(len(lst)-1)//2].key < parent.key:
            parent.left = lst[(len(lst)-1)//2]
        else:
            parent.right = lst[(len(lst)-1)//2]
    fix_size(lst[(len(lst)-1)//2])


def fix_size(node: Node) -> int:
    if node is None:
        return -1
    x = 1 + fix_size(node.left) + 1 + fix_size(node.right)
    node.size = x + 1
    return x


def bin_search(lst: List[Node],
               min: int, max: int,
               last: Optional[Node]) -> Optional[Node]:
    if min > max:
        return None
    middle = (min + max) // 2
    lst[middle].parent = last
    lst[middle].left = bin_search(lst, min, middle - 1, lst[middle])
    lst[middle].right = bin_search(lst, middle + 1, max, lst[middle])
    return lst[middle]


def inorder(node: Optional[Node],
            lst: List[Node]) -> None:
    if node is None:
        return None
    inorder(node.left, lst)
    lst.append(node)
    inorder(node.right, lst)