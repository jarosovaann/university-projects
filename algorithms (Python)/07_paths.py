from heapq import heappush, heappop  # prioritni fronta
from typing import List, Tuple
import math


class Graph:
    def __init__(self, size: int):
        self.size = size
        self.succs: List[List[Tuple[int, int]]] = \
            [[] for _ in range(size)]


# Part 1.
"""
    Description: An implementation of Dijkstra's algorithm
    
    Time complexity: O((|Vertices| + |Edges|) · log |Vertices|)
    Extra space complexity: O(|Vertices| + |Edges|)

    Input: Non-negative weighted and directed graph, start vertex, initial energy to travel through graph

    Output: List of reachable vertices 
"""


def reachable(graph: Graph, start: int, initial_energy: int) -> List[int]:

    paths = [math.inf] * graph.size
    paths[start] = 0
    seen = [False] * graph.size
    heap: List[Tuple[float, int]] = []
    heappush(heap, (paths[start], start))
    while heap:
        distance, actual = heappop(heap)
        if seen[actual]:
            continue
        seen[actual] = True
        for i in graph.succs[actual]:
            if paths[i[1]] > paths[actual] + i[0] and initial_energy >= paths[actual] + i[0]:
                paths[i[1]] = paths[actual] + i[0]
                seen[i[1]] = False
                heappush(heap, (paths[i[1]], i[1]))
    reach = []
    for vertex, lenn in enumerate(paths):
        if initial_energy >= lenn:
            reach.append(vertex)
    return reach


# Part 2.
"""
    Description: An implementation of Bellman-Ford's algorithm

    Time complexity: O(|Vertices| · (|Vertices| + |Edges|))
    Extra space complexity: O(|Vertices|)

    Input: Weighted and directed graph (negative weights possible), start vertex, initial energy to travel through graph

    Output: List of reachable vertices
"""


def reachable_with_charging(graph: Graph, start: int, initial_energy: int) \
        -> List[int]:

    paths = [math.inf] * graph.size
    paths[start] = 0
    for x in range(graph.size - 1):
        for actual in range(graph.size):
            for i in graph.succs[actual]:
                if paths[i[1]] > paths[actual] + i[0] \
                        and paths[actual] + i[0] <= initial_energy:
                    paths[i[1]] = paths[actual] + i[0]
    to_search: List[int] = []
    for actual in range(graph.size):
        for i in graph.succs[actual]:
            if paths[i[1]] > paths[actual] + i[0] \
                    and paths[actual] + i[0] <= \
                    initial_energy:
                to_search.append(actual)
    reach: List[int] = []
    for x in to_search:
        DFS(graph, x, paths)
    for vertex, lenn in enumerate(paths):
        if initial_energy >= lenn:
            reach.append(vertex)
    return reach


def DFS(graph: Graph, node: int, paths) -> None:
    if paths[node] == -math.inf:
        return
    paths[node] = -math.inf
    for lenn, vertex in graph.succs[node]:
        if paths[vertex] != -math.inf:
            DFS(graph, vertex, paths)
