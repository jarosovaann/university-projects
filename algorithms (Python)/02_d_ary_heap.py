from typing import List, Tuple, Optional
from math import inf

"""
DMinHeap is a data structure, DMinHeap has its arity, can be described as k-arity tree 
(instead of link to sons, all values are stored in array)
Minimal value is stored at first position of the array (heap.array[0]),
all other values must be greater or equal, this rules applies for all 'levels' of heap
"""


class DMinHeap:
    def __init__(self, array: List[int], arity: int):
        self.array = array
        self.arity = arity


# Part 1.
"""
    Description: This function checks if DMinHeap is correct (as described above)
    
    Time complexity: O(len(heap))
    Extra space complexity: O(1)

    Input: DMinHeap
    
    Output: True if DMinHeap is correct, False otherwise
"""


def is_correct(heap: DMinHeap) -> bool:
    for i in range(len(heap.array)-1, 0, -1):
        if not check_parrent(i, heap):
            return False
    return True


def check_parrent(index: int, heap: DMinHeap) -> bool:
    if heap.array[(index-1)//heap.arity] > heap.array[index]:
        return False
    return True


# Part 2.
"""
    Description: Function fix DMinHeap down from given index

    Time complexity: O(heap.arity * height of the subtree (from given index))
    Extra space complexity: O(height of the subtree (from given index))
    
    Input: DMinHeap, index (0 to len(heap.array) - 1)
    
    Output: None, DMinHeap is correct down from given index
"""


def heapify(heap: DMinHeap, index: int) -> None:
    i_smaller = check_smaller(heap, index)
    if i_smaller is None:
        return None
    swap(i_smaller, index, heap)


def check_smaller(heap: DMinHeap,
                  index: int) -> Optional[int]:
    child_index = (index + 1) * heap.arity
    smallest = heap.array[index]
    smallest_index = None
    if child_index - (heap.arity - 1) >= len(heap.array):
        return None
    for i in range(heap.arity):
        actual_index = child_index - i
        if actual_index < len(heap.array):
            if heap.array[actual_index] < smallest:
                smallest = heap.array[actual_index]
                smallest_index = actual_index
    return smallest_index


def swap(i_smaller: int, index: int, heap: DMinHeap) -> None:
    hold = heap.array[index]
    heap.array[index] = heap.array[i_smaller]
    heap.array[i_smaller] = hold
    heapify(heap, i_smaller)


# Part 3.
"""
    Description: Function changes arity of heap and shuffles values in heap.array so that heap is correct
    
    Time complexity: O(len(heap.array))
    Extra space complexity: O(log len(heap.array))


    Input: correct DMinHeap, new arity (integer)
    
    Output: None, DMinHeap has new arity and is correct
"""


def change_arity(heap: DMinHeap, new_arity: int) -> None:
    heap.arity = new_arity
    for i in range(len(heap.array)-1, -1, -1):
        heapify(heap, i)


# Part 4.
"""
    Description: Function returns three minimal values of DMinHeap
    
    Time complexity: O(heap.arity)
    Extra space complexity: O(1)
    
    Input: correct DMinHeap (with at least 3 values in array)  

    Output: Tuple of three minimal values (ascending order)
"""


def min_three(heap: DMinHeap) -> Tuple[int, int, int]:
    candidate1 = inf
    candidate2 = inf
    candidate1, candidate1_index = find_candidate(heap, candidate1, heap.arity)
    for i in range(1, heap.arity+1):
        if i >= len(heap.array):
            break
        if heap.array[i] < candidate2 and i != candidate1_index:
            candidate2 = heap.array[i]
    a = (candidate1_index + 1) * heap.arity
    candidate2, candidate2_index = find_candidate(heap, candidate2, a)
    return heap.array[0], candidate1, candidate2


def find_candidate(heap: DMinHeap, candidate,
                   child_index: int) \
        -> Tuple[int, Optional[int]]:
    candidate_index = None
    for i in range(heap.arity-1, -1, -1):
        actual_index = child_index - i
        if actual_index >= len(heap.array):
            break
        if heap.array[actual_index] < candidate:
            candidate = heap.array[actual_index]
            candidate_index = actual_index
    return candidate, candidate_index