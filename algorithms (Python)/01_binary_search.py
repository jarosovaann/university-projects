from typing import List, Tuple


# Part 1.
"""
    Description: Function splits given list of numbers (list is in ascending order)
            to smaller numbers than given key, numbers that equal to given key
            and numbers that are greater than given key.
            Function is using binary search

    Time complexity: O(log len(numbers))
    
    Input: list of numbers in ascending order, key
    
    Output: Tuple[greatest index where number < key, smallest index where number > key]
"""


def partition(numbers: List[int], key: int) -> Tuple[int, int]:
    len_nums = len(numbers)
    if len_nums == 0:
        return 0, 0
    index = len_nums // 2
    max = len_nums - 1
    min = 0
    left = find_partition_left(index, numbers, key, max, min, len_nums)
    right = find_partition_right(index, numbers, key, max, min, len_nums)
    return left, right


def find_partition_right(index: int, numbers: List[int],
                         key: int, max: int, min: int, len_nums: int) -> int:
    if numbers[-1] <= key:
        right = len_nums
    elif numbers[0] > key:
        right = 0
    else:
        right = partition_rec_right(index, numbers, key, max, min)
    return right


def find_partition_left(index: int, numbers: List[int],
                        key: int, max: int, min: int, len_nums: int) -> int:
    if numbers[-1] < key:
        left = len_nums
    elif numbers[0] >= key:
        left = 0
    else:
        left = partition_rec_left(index, numbers, key, max, min)
    return left


def partition_rec_left(index: int, numbers: List[int],
                       key: int, max: int, min: int) -> int:
    if max == min:
        return max
    if numbers[index] < key:
        return partition_rec_left((index + max) // 2, numbers,
                                  key, max, index+1)
    return partition_rec_left((index + min) // 2, numbers, key, index, min)


def partition_rec_right(index: int, numbers: List[int],
                        key: int, max: int, min: int) -> int:
    if max == min:
        return max
    if numbers[index] > key:
        return partition_rec_right((index + min) // 2, numbers,
                                   key, index, min)
    return partition_rec_right((index + max) // 2, numbers, key, max, index+1)


# Part 2.
"""
    Description: Function finds local minima in list
        which contains one local minima and numbers in list are mutually exclusive
    
    Time complexity: O(log len(numbers))
    
    Input: List of numbers containing one local minima, numbers are mutually exclusive
    
    Output: Local minima of given list
"""


def minimum(numbers: List[int]) -> int:
    len_nums = len(numbers)
    if len_nums == 1:
        return numbers[0]
    if numbers[0] < numbers[1]:
        return numbers[0]
    if numbers[-1] < numbers[-2]:
        return numbers[-1]
    index = len_nums // 2
    max = len_nums - 1
    min = 0
    return find_min(numbers, index, max, min)


def find_min(numbers: List[int], index: int, max: int, min: int) -> int:
    actual = numbers[index]
    if actual < numbers[index+1] and actual < numbers[index-1]:
        return actual
    if numbers[index+1] < actual:
        return find_min(numbers, (index + max) // 2, max, index)
    return find_min(numbers, (index + min) // 2, index, min)
