from typing import List


class Graph:
    def __init__(self, size: int):
        self.size = size
        self.succs: List[List[int]] = [[] for _ in range(size)]


# Part 1.
"""
    Description: An implementation of Kosaraju-Sharir's algorithm

    Time complexity: O(|Vertices| + |Edges|)

    Input: Directed graph

    Output: List of strongly connected components of graph
"""


def strongly_connected_components(graph: Graph) -> List[List[int]]:
    l = ["white"] * graph.size
    n: List[int] = []

    for i in range(graph.size):
        if l[i] == "white":
            l[i] = 'gray'
            dfs(graph, i, l, n)
    transposed = Graph(graph.size)

    transpose(graph, transposed)
    l = ["white"] * graph.size
    ret = []
    for i in n[::-1]:
        components: List[int] = []
        if l[i] == 'white':
            l[i] = 'gray'
            dfs2(transposed, i, l, components)
            ret.append(components)
    return ret


def transpose(graph: Graph,
              transposed: Graph) -> None:
    for i in range(graph.size):
        for x in graph.succs[i]:
            transposed.succs[x].append(i)


def dfs2(graph: Graph,
         node: int, l: List[str],
         components: List[int]) -> None:
    components.append(node)
    for i in graph.succs[node]:
        if l[i] == 'white':
            l[i] = 'gray'
            dfs2(graph, i, l, components)
    l[node] = 'black'


def dfs(graph: Graph,
        node: int, l: List[str], n: List[int])\
        -> None:
    for i in graph.succs[node]:
        if l[i] == "white":
            l[i] = "gray"
            dfs(graph, i, l, n)
    l[node] = "black"
    n.append(node)


# Part 2.
"""
    Description: An implementation of Kosaraju-Sharir's algorithm
        using DFS to find terminal strongly connected components
    
    Time complexity: O(|Vertices| + |Edges|)
    
    Input: Directed graph
    
    Output: List of terminal strongly connected components (a terminal component is a strongly connected component
        from which doesn't lead a path to another strongly connected component)
"""


def terminal_sccs(graph: Graph) -> List[List[int]]:
    ret = []
    l = ['white'] * graph.size
    for component in strongly_connected_components(graph)[::-1]:
        for x in component:
            l[x] = 'gray'
        if lists(graph, component[0], l, True):
            ret.append(component)
        for x in component:
            l[x] = 'white'
    return ret


def lists(graph: Graph,
          node: int, l: List[str], term: bool) -> bool:
    for i in graph.succs[node]:
        if l[i] == 'white':
            return False
        elif l[i] == 'gray':
            l[i] = 'black'
            if not lists(graph, i, l, term):
                term = False
    return term


# Part 3.
"""
    Description: An implementation of Kosaraju-Sharir's algorithm
        using DFS to find initial strongly connected components

    Time complexity: O(|Vertices| + |Edges|)

    Input: Directed graph

    Output: List of initial strongly connected components (an initial component is a strongly connected component
        to which doesn't lead a path from another strongly connected component)
"""


def initial_sccs(graph: Graph) -> List[List[int]]:
    transposed = Graph(graph.size)
    transpose(graph, transposed)
    return terminal_sccs(transposed)
