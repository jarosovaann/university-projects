from typing import List, Optional, TextIO
from math import inf


"""
Left minimal tree:
    - all leaves are in same depth
    tree is left aligned:
        * if there is an only son it's supposed to be the left son
        * if a node has both sons, 
        the left one is a parent of a perfect tree (subtree has always both sons with exception for leaves)
    - all keys of an inner nodes are minima of their subtrees (the values are stored in leaves)
"""


class Node:
    def __init__(self, key: int,
                 left: Optional['Node'] = None,
                 right: Optional['Node'] = None):
        self.key = key
        self.left = left
        self.right = right


class BinTree:
    def __init__(self, root: Optional[Node] = None):
        self.root = root


# Part 1.
"""
    Description: Function builds a left minimal tree with smallest possible depth
    
    Time complexity: O(nodes)
    
    Input: List of leaves keys
    
    Output: Left minimal BinTree built from the input
"""


def build_min_tree(leaves: List[int]) -> BinTree:
    if len(leaves) == 1:
        return BinTree(Node(leaves[0]))
    if len(leaves) == 0:
        return BinTree()
    layer = assign_parent(leaves)
    while len(layer) > 1:
        layer = assign_node(layer)
    return BinTree(layer[0])


def assign_parent(leaves: List[int]) -> List[Node]:
    hold = []
    i = 0
    while i < len(leaves):
        if i + 1 < len(leaves):
            parent = Node(min(leaves[i], leaves[i+1]))
            parent.left = Node(leaves[i])
            parent.right = Node(leaves[i+1])
            i += 2
        else:
            parent = Node(leaves[i])
            parent.left = Node(leaves[i])
            i += 1
        hold.append(parent)
    return hold


def assign_node(node_list: List[Node]) -> List[Node]:
    hold = []
    i = 0
    while i < len(node_list):
        if i + 1 < len(node_list):
            parent = Node(min(node_list[i].key, node_list[i+1].key))
            parent.left = node_list[i]
            parent.right = node_list[i+1]
            i += 2
        else:
            parent = Node(node_list[i].key)
            parent.left = node_list[i]
            i += 1
        hold.append(parent)
    return hold


# Part 2.
"""
    Description: Function check if all leaves are in the same depth

    Time complexity: O(nodes)

    Input: BinTree
    
    Output: True if all leaves are in the same depth, False otherwise
"""


def check_leaf_depth(tree: BinTree) -> bool:
    return check_depth_rec_max(tree.root) == check_depth_rec_min(tree.root)


def check_depth_rec_max(node: Optional[Node]) -> int:
    if node is None:
        return -1
    if node.left is None and node.right is not None:
        return check_depth_rec_max(node.right) + 1
    if node.right is None and node.left is not None:
        return check_depth_rec_max(node.left) + 1
    return max((check_depth_rec_max(node.left) + 1),
               (check_depth_rec_max(node.right) + 1))


def check_depth_rec_min(node: Optional[Node]) -> int:
    if node is None:
        return -1
    if node.left is None and node.right is not None:
        return check_depth_rec_min(node.right) + 1
    if node.right is None and node.left is not None:
        return check_depth_rec_min(node.left) + 1
    return min((check_depth_rec_min(node.left) + 1),
               (check_depth_rec_min(node.right) + 1))


"""
    Description: Function checks if tree is left aligned (description above)
    
    Time complexity: O(height of tree)
    
    Input: Tree where all leaves are in the same depth 

    Output: True if tree is left aligned, False otherwise
"""


def check_left_align(tree: BinTree) -> bool:
    if not check_left_child(tree.root):
        return False
    return left_perfect(tree.root, False)


def check_left_child(node: Optional[Node]) -> bool:
    if node is None:
        return True
    if node.left is None and node.right:
        return False
    return check_left_child(node.left) and check_left_child(node.right)


def left_perfect(node: Optional[Node], left: bool) -> bool:
    if node is None:
        return True
    if left and node.left is None and node.right is not None:
        return False
    if left and node.right is None and node.left is not None:
        return False
    if node.left is not None and node.right is not None:
        return left_perfect(node.left, True) and left_perfect(node.right, left)
    return left_perfect(node.left, left) and left_perfect(node.right, left)


"""
    Description: Function check if in all inner nodes stores a minima of it's subtree
    
    Time complexity: O(nodes)
    
    Input: Tree where all leaves are in same depth and is left aligned
    
    Output: True if all inner nodes stored minima of their subtrees, False otherwise
    """


def check_min(tree: BinTree) -> bool:
    if tree.root is None:
        return True
    if min_rec(tree.root) is None:
        return False
    return True


def min_rec(node):
    if node.left is None and node.right is None:
        return node.key
    elif node.left is None and node.right is not None:
        key_r = min_rec(node.right)
        key_l = inf
    elif node.right is None and node.left is not None:
        key_l = min_rec(node.left)
        key_r = inf
    else:
        key_r = min_rec(node.right)
        key_l = min_rec(node.left)
    if key_l is None or key_r is None:
        return None
    if node.key != min(key_r, key_l):
        return None
    return node.key