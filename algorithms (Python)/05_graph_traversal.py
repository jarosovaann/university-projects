from collections import deque

from typing import List, Optional, Tuple, Deque


class Vertex:
    def __init__(self, name: str):
        self.name = name
        self.succs: List[Vertex] = []
        self.flag: Optional[int] = None


# Part 1.
"""
    Description: An implementation of Breadth First Search algorithm to find reachable part of directed graph
    
    Time complexity: O(|Vertices| + |Edges|)
    Extra space complexity: O(|Vertices|)
    
    Input: Start vertex

    Output: A tuple[number of reachable vertices, number of reachable edges]
"""


def reachable_size(source: Vertex) -> Tuple[int, int]:
    source.flag = 1
    q: Deque[Vertex] = deque()
    q.append(source)
    vertex = 0
    edge = 0
    while q:
        actual = q.popleft()
        for i in actual.succs:
            edge += 1
            if i.flag is None:
                q.append(i)
                i.flag = 1
        vertex += 1
    return vertex, edge


# Part 2.
"""
    Description: An implementation of Depth First Search algorithm to find if reachable part of directed graph
    contains cycle
    
    Time complexity: O(|Vertices| + |Edges|)
    Extra space complexity: O(|Vertices|)
    
    Input: Start vertex
    
    Output: True if reachable part of graph contains cycle, False otherwise 
"""


def has_cycle(source: Vertex) -> bool:
    return DFS(source, False)


def DFS(vertex: Vertex, cycle: bool) -> bool:
    vertex.flag = 1
    for i in vertex.succs:
        if i.flag is None:
            if DFS(i, cycle):
                cycle = True
        else:
            if i.flag == 1:
                return True
    vertex.flag = 2
    return cycle


# Part 3.
"""
    Description: An implementation of Breadth First Search algorithm to check if directed graph is a tree
    
    Time complexity: O(|Vertices| + |Edges|)
    Extra space complexity: O(|Vertices|)
    
    Input: Start vertex
    
    Output: True if reachable graph is a tree, False otherwise
"""


def is_tree(source: Vertex) -> bool:
    source.flag = 1
    q: Deque[Vertex] = deque()
    q.append(source)
    while q:
        actual = q.popleft()
        for i in actual.succs:
            if i.flag is not None:
                return False
            else:
                q.append(i)
                i.flag = 1
    return True


# Part 4.
"""
    Description: An implementation of Breadth First Search algorithm to check if directed graph is a tree
    
    Time complexity: O(|Vertices| + |Edges|)
    Extra space complexity: O(|Vertices|)

    Input: Start vertex, target vertex
    
    Output: distance from start to target, None if from start to target doesn't exist a path
"""


def distance(source: Vertex, target: Vertex) -> Optional[int]:
    source.flag = 0
    q: Deque[Vertex] = deque()
    q.append(source)
    while q:
        actual = q.popleft()
        if actual == target:
            return actual.flag
        for i in actual.succs:
            if i.flag is None:
                q.append(i)
                i.flag = actual.flag + 1
    return None
