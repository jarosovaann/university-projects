﻿using System;

namespace oop
{
    class RPGCharacter{
        private int armourClass;
        private int dexterity;

        public RPGCharacter(int armourClass, int dexterity){
            this.armourClass = armourClass;
            this.dexterity = dexterity;
        }

        public int ArmourClass(){
            return armourClass;
        }

        public int Dexterity(){
            return dexterity;
        }

        public virtual float DefenceNumber(){
            return Dexterity() + ArmourClass();
        }

        public virtual CharacterRace Race(){
            return CharacterRace.Unknown;
        }

        public bool IsNewChar()
        {
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("ready to create some heroes!");
        }
    } 

    class Hero : RPGCharacter{
        private int experience;

        public Hero(int armourClass, int dexterity) : base(armourClass, dexterity)
        {
            this.experience = 1;
        }
    
        public Hero(int armourClass, int dexterity, int experience) : base(armourClass, dexterity)
        {
            this.experience = experience;
        }

        public int Experience()
        {
            return experience;
        }

        public override float DefenceNumber(){
            return base.DefenceNumber() * Experience();
        }

        public override CharacterRace Race(){
            return CharacterRace.Human;
        }   
        public new bool IsNewChar()
        {
            return true;
        }
    }

    abstract class BetterCharacter : RPGCharacter{
        private int intelligence;

        protected BetterCharacter(int armourClass, int dexterity, int intelligence) 
            : base(armourClass,dexterity){
            this.intelligence = intelligence;
        }

        public int Intelligence(){
            return intelligence;
        }
        public override abstract CharacterRace Race();
        public new bool IsNewChar()
        {
            return true;
        }
    }

    class Orc : BetterCharacter {
        public Orc(int armourClass, int dexterity, int intelligence) : base(armourClass, dexterity, 
        intelligence){
        }

        public override float DefenceNumber(){
            return 1.5f * (base.DefenceNumber() + base.Intelligence());
        }
        public override CharacterRace Race(){
            return CharacterRace.Orc;
        }
    }

    class Dwarf : BetterCharacter {
        public Dwarf(int armourClass, int dexterity, int intelligence) : base(armourClass, dexterity, 
        intelligence){
        }
        public override float DefenceNumber(){
            return 2.0f * (base.DefenceNumber() + base.Intelligence());
        }
        public override CharacterRace Race(){
            return CharacterRace.Dwarf;
        }
    }

    class Elf : BetterCharacter {
        public Elf(int armourClass, int dexterity, int intelligence) : base(armourClass, dexterity, 
        intelligence){
        }
        public override float DefenceNumber(){
            return 0.8f * (base.DefenceNumber() + base.Intelligence());
        }
        public override CharacterRace Race(){
            return CharacterRace.Elf;
        }
    }

    enum CharacterRace { Human, Orc, Dwarf, Elf, Unknown }
}
