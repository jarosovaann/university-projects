﻿using System;
using System.Collections.Generic;

namespace oop
{

    interface ITree<T>
    {
        T Value();
        IEnumerable<T> Leaves();
        IEnumerable<ITree<T>> Children();
    }

    class MyTree<T> : ITree<T>, IEquatable<MyTree<T>>{

        private T value;
        private MyTree<T>[] children;


        public MyTree(T t){
            this.value = t;
            this.children = new MyTree<T>[0];
        }
        public MyTree(T t, MyTree<T>[] children){
            this.children = children;
            this.value = t;
        }

        public T Value(){
            return this.value;
        }

        public IEnumerable<T> Leaves(){ 
            if (this == null || this.children == null){
                return null;
            }
            var nodes = new List<T>();
            if ((this.children).Length == 0){
                nodes.Add(this.value);
            }
            foreach (var myTree in this.children){
                if (myTree != null){
                    nodes.AddRange(myTree.Leaves());
                } 
            }
            return nodes;
        }

        public IEnumerable<ITree<T>> Children(){
            if (this == null){
                return null;
            }
            var nodes = new List<ITree<T>>();
            foreach (var myTree in this.children){
                if (myTree != null){
                    nodes.Add(myTree);
                } 
            }
            return nodes;
        }


        public override String ToString(){
            String s = "";
            if (this == null){
                return "";
            }
            if (this.children.Length > 0){
                s += ':';
            }
            foreach (MyTree<T> myTree in this.children){
                if (myTree == null){
                    s += "[]";
                    continue;
                }
                else{
                    s += myTree.ToString();
                }
            }
            return '[' + (this.value).ToString()+ s +  ']';
        }


        public bool Equals(MyTree<T> t){
            if (t == null){
                return false;
            }
            return this.ToString() == t.ToString();
        }

        public override bool Equals(object obj) {
            if (obj == null){
                return false;
            }
            if (!(this.GetType().Equals(obj.GetType()))){
                return false;
            }
            var other = (MyTree<T>) obj;
            return this.Equals(other);
        }

    }


    class Program
    {
        static String PrintIterable<T, E>(T list) where T : IEnumerable<E>
        {
            String result = "[";
            bool first = true;
            foreach (E item in list)
            {
                if (!first) result += ", ";
                first = false;
                result += (item);
            }
            result += "]";
            return result;

        }
        static void Main(string[] args)
        {
            ITree<char> t6b = new MyTree<char>('a', new MyTree<char>[] { new MyTree<char>('b', new MyTree<char>[] { new MyTree<char>('c', new MyTree<char>[] { new MyTree<char>('d', new MyTree<char>[] { new MyTree<char>('e') }) }), null }) });
            System.Console.WriteLine(t6b.ToString());
        }
    }
}