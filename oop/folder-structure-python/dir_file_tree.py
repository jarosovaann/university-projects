from typing import Dict, List, Optional, Set, Tuple


class Node:
    def __init__(self, nid: int, name: str, owner: str,
                 children_nid: List[int]) -> None:
        self.nid = nid
        self.name = name
        self.owner = owner
        self.is_dir = True
        self.size = 0
        self.parent: Optional['Node'] = None
        self.children: List['Node'] = []
        self.children_nid = children_nid

    def is_valid(self) -> bool:
        root = self.get_root()
        return root.is_valid_node(root)

    def is_valid_node(self, root: 'Node') -> bool:
        if '/' in self.name or self.owner == "" or \
                (self.name == "" and self.nid != root.nid):
            return False
        if self.is_dir:
            children_names: Set[str] = set()
            valid = self.valid_children(children_names, root)
            return valid
        return True

    def valid_children(self, children_names: Set[str], root: 'Node') -> bool:
        for children in self.children:
            if not children.is_valid_node(root):
                return False
            if children.name in children_names:
                return False
            children_names.add(children.name)
        return True

    def get_root(self) -> 'Node':
        if self.parent is None:
            return self
        else:
            return self.parent.get_root()

    def draw(self) -> None:
        self.draw_childs("")  # one more parametr needed

    def draw_childs(self, start: str) -> None:
        print(start+"--", self.name)
        if "\\" in start:
            start = start.replace("\\", " ")
        start += "   |"
        leng = len(self.children)
        for index, child in enumerate(self.children):
            if index == leng - 1:
                start = start[:-1] + "\\"
            child.draw_childs(start)

    def full_path(self) -> str:
        path = ""
        if self.is_dir is True:
            path = "/"
        if self.parent is not None:
            path = self.name + path
            path = self.parent.full_path() + path
            return path
        return path

    def disk_usage(self) -> Tuple[int, int]:
        usage = 0, 0
        if not self.is_dir:
            return 1, self.size
        for children in self.children:
            if not children.is_dir:
                usage = usage[0] + 1, usage[1] + children.size
            else:
                children_usage = children.disk_usage()
                usage = usage[0] + children_usage[0], \
                    usage[1] + children_usage[1]
        return usage

    def all_owners(self) -> Set[str]:
        owners = {self.owner}
        for child in self.children:
            owners.update(child.all_owners())
        return owners

    def empty_files(self) -> List['Node']:
        empties = []
        if not self.is_dir and self.size == 0:
            empties.append(self)
            return empties
        for child in self.children:
            empties.extend(child.empty_files())
        return empties

    def prepend_owner_name(self) -> None:
        if not self.is_dir:
            self.name = self.owner + "_" + self.name
        for child in self.children:
            child.prepend_owner_name()

    def add_keep_helper(self, start: int) -> int:
        make_child: List['Node'] = []
        if not self.children and self.is_dir:
            self.children = make_child
            made_child = Node(start, ".keep", self.owner, [])
            made_child.parent = self
            made_child.is_dir = False
            make_child.append(made_child)
            start += 1
            return start
        for child in self.children:
            start = child.add_keep_helper(start)
        return start

    def add_keep_files(self, start: int) -> None:
        self.add_keep_helper(start)
        # helper func bsc func add_keep_files has to return None

    def remove_empty_dirs(self) -> None:
        for child in self.children[::-1]:
            child.remove_empty_dirs()
            if child.is_dir and not child.children:
                self.children.remove(child)

    def remove_all_foreign(self, user: str) -> None:
        for child in self.children[::-1]:
            if child.owner != user:
                self.children.remove(child)
            else:
                child.remove_all_foreign(user)


def create_nodes(metadata: Dict[int, Tuple[str, str]],
                 file_sizes: Dict[int, int],
                 dir_content: Dict[int, List[int]],
                 all_nodes: Dict[int, 'Node']) -> Optional[Tuple[int, int]]:
    f_s_len = 0
    d_c_len = 0
    for meta_nid in metadata:
        names = metadata.get(meta_nid)
        if names is None:  # check for mypy
            return None
        name = names[0]
        owner = names[1]
        node = (Node(meta_nid, name, owner, []))

        if meta_nid in file_sizes:
            if meta_nid in dir_content:
                return None
            node.size = file_sizes[meta_nid]
            node.is_dir = False
            f_s_len += 1

        elif meta_nid in dir_content:
            node.children_nid = dir_content[meta_nid]

            d_c_len += 1
            for child_nid in dir_content[meta_nid]:
                if child_nid not in metadata:
                    return None

        all_nodes.update({meta_nid: node})
        # creating dict for better orientation
    return f_s_len, d_c_len
    # f_s_len and d_c_len can tell if metadata contains
    # all nids from file_sizes and dir_content


def build_fs(metadata: Dict[int, Tuple[str, str]],
             file_sizes: Dict[int, int],
             dir_content: Dict[int, List[int]]) -> Optional[Node]:
    all_nodes: Dict[int, 'Node'] = {}
    valid = create_nodes(metadata, file_sizes, dir_content, all_nodes)
    if valid is None:
        return None
    f_s_len, d_c_len = valid
    if f_s_len != len(file_sizes) or d_c_len != len(dir_content):
        return None
    if len(all_nodes) >= 1:
        tree = structure(all_nodes)
        return find_root(tree)
    return None


def structure(data: Dict[int, 'Node']) -> Dict[int, 'Node']:
    if len(data) >= 2:
        for node in data.values():
            if node.children_nid:
                for nid_child in node.children_nid:
                    connect_node(node, nid_child, data)
    return data


def connect_node(parent: 'Node', nid_child: 'int',
                 data: Dict[int, 'Node']) -> None:
    child = data.get(nid_child)
    if child is None:  # check for mypy
        return None
    child.parent = parent
    parent.children.append(child)


def find_root(data: Dict[int, 'Node']) -> Optional['Node']:
    count = 0
    root = None
    for candidate in data.values():
        if candidate.parent is None:
            root = candidate
            count += 1
        if count > 1:
            return None  # no need to go through the rest
    return root
