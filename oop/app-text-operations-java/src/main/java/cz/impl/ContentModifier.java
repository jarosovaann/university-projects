package cz.impl;

import cz.FileLoader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

/**
 * class modifiing content of a file
 * @author Anna Jarosova
 */

public class ContentModifier {
    private List<String> content;

    /**
     *
     * @param path - path to file
     * @throws IOException - if path is not valid
     */
    public ContentModifier(String path) throws IOException {
        this.content = new FileLoader().loadAsLines(path);
    }

    /**
     * getter
     * @return content
     */
    public List<String> getContent() {
        return content;
    }

    /**
     * sorts naturally content
     */
    public void sortNatural(){
        content.sort(Comparator.naturalOrder());
    }

    /**
     * changes content to contain duplicates only
     */
    public void getDuplicities(){
        Set<String> uniqueLines = new HashSet<>();
        List<String> duplicates = new ArrayList<>();
        for (String line : content){
            if (!uniqueLines.add(line)) {
                duplicates.add(line);
            }
        }
        this.content = duplicates;
    }

    /**
     * changes content to contain unique lines only
     */
    public void getUnique(){
        List<String> uniqueLines = new ArrayList<>();
        for (String line : content){
            if (!uniqueLines.contains(line)) {
                uniqueLines.add(line);
            }
        }
        this.content = uniqueLines;
    }
}
