package cz.impl;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import cz.TerminalOperation;
import cz.cmd.TerminalOperationConverter;

import java.io.IOException;


/**

 * @author Anna Jarosova
 */
public class Application {

    @Parameter(names = {"--file"}, required = true)
    private String path;

    @Parameter(converter = TerminalOperationConverter.class)
    private TerminalOperation operation = TerminalOperation.LINES;
    @Parameter(names = {"-u"})
    private boolean unique;

    @Parameter(names = {"-s"})
    private boolean sort;

    @Parameter(names = {"-d"})
    private boolean duplicate;

    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     */
    public static void main(String[] args) {
        Application app = new Application();


        JCommander instr = JCommander.newBuilder()
                .addObject(app)
                .build();

        instr.parse(args);

        app.run(instr);
    }

    /**
     * Application runtime logic
     *
     * @param cli
     */
    @SuppressWarnings("ConstantConditions")
    private void run(JCommander cli) {

        ContentModifier lines;

        try {
            lines = new ContentModifier(path);
        } catch (IOException e) {
            System.err.println("Unable to process file '" + path + "'!");
            return;
        }

        if ((duplicate && unique)){
            System.err.print("Invalid combination of options was used!");
            return;
        }

        if (unique || operation == TerminalOperation.SIMILAR){
            lines.getUnique();
        }
        if(sort){
            lines.sortNatural();
        }
        if(duplicate){
            lines.getDuplicities();
        }
        PrintOperation content = new PrintOperation(lines.getContent());

        switch (operation){
            case COUNT:
                content.printCountLines();
                break;
            case SIZES:
                content.printSizes();
                break;
            case SIMILAR:
                content.levenshteinDistance();
                break;
            default:
                content.printLinesOnly();
        }
    }
}
