package cz.impl;

import java.util.List;

/**
 * class printing formated text
 * @author Anna Jarosova
 */

public class PrintOperation {
    private final List<String> content;

    /**
     *
     * @param content - content (might be mofidied by ContentModifier)
     */
    public PrintOperation(List<String> content){
        this.content = content;
    }

    /**
     * prints all lines
     */
    public void printLinesOnly() {
        for (String line : content){
            System.out.println(line);
        }
    }

    /**
     * prints count of lines
     */
    public void printCountLines() {
        System.out.println(content.size());
    }

    /**
     * prints size of each line
     */
    public void printSizes() {
        for (String line : content){
            System.out.println(line.length() + ": " + line);
        }
    }

    /**
     * calculates and prints smallest Levenshtein distance between two lines
     */
    public void levenshteinDistance() {
        int countDistance;
        int countDistance2 = Integer.MAX_VALUE;
        int indexOfLines1 = 0;
        int indexOfLines2 = 1;
        for (int i = 0; i < content.size(); i++) {
            for (int j = (i + 1); j < content.size(); j++){
                countDistance = LevenshteinDistance.distance(content.get(i), content.get(j));
                if (countDistance < countDistance2){
                    countDistance2 = countDistance;
                    indexOfLines1 = i;
                    indexOfLines2 = j;
                }
            }
        }
        System.out.println("Distance of " + countDistance2);
        System.out.println(content.get(indexOfLines1) + " ~= " + content.get(indexOfLines2));
    }
}
