package cz.impl;

/**
 * class computing Levenshtein distance between two strings
 * @author Anna Jarosova
 */

public class LevenshteinDistance{

    /**
     *
     * @param first string to be compared
     * @param second string to be compared
     * @return distance between first and second
     */
    public static int distance(String first, String second) {
        int [] price = new int [second.length()];
        for (int j = 1; j <= price.length; j++){
            price[j - 1] = j;
        }
        for (int i = 0; i < first.length(); i++) {
            int actual = i;
            for (int j = 0; j < second.length(); j++) {
                if (!(first.charAt(i) == second.charAt(j))) {
                    actual++;
                }
                int hold = Math.min((1 + Math.min(price[j], i)), actual);
                actual = price[j];
                price[j] = hold;
            }
        }
        return price[second.length() - 1];
    }
}


