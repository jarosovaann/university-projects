package cz;

import java.util.Objects;

/**
 * Terminal Operations
 */
public enum TerminalOperation {
    LINES,
    COUNT,
    SIZES,
    SIMILAR;


    /**
     *
     * @param name name of the operation
     * @return {@link TerminalOperation} instance
     */
    public static TerminalOperation forName(String name) {
        if (Objects.equals(name, "count")) {
            return TerminalOperation.COUNT;
        }
        if (Objects.equals(name, "similar")) {
            return TerminalOperation.SIMILAR;
        }
        return TerminalOperation.LINES;
    }
}
