package cz;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * This class works with files and paths.
 */
public class FileLoader {

    public List<String> loadAsLines(String path) throws IOException {
        InputStream inputStream = Files.newInputStream(Paths.get(path));

        return new BufferedReader(new InputStreamReader(inputStream,
                StandardCharsets.UTF_8)).lines().collect(Collectors.toList());

    }
}
