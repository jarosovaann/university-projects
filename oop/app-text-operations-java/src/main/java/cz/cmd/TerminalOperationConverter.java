package cz.cmd;

import com.beust.jcommander.IStringConverter;
import cz.TerminalOperation;

/**
 * Converter for terminal operation command line option
 */
public class TerminalOperationConverter implements IStringConverter< TerminalOperation > {

   @Override
   public TerminalOperation convert(String value) {
       return TerminalOperation.forName(value);
   }
}

