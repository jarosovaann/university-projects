# Java text operation


## About project
This code is writen in java with Apache Maven for builds.
App performs several operations on text files, these are described down below.
Use test-file.txt file from the repository.
File can be formatted E.g.:

```
Line of text
Line number two
Another line
Line saying nothing
Line of text
Line of text
```


## Creating and running executable jar file

With command **mvn clean install** projects builds and creates folder target with application.jar

To correctly run the jar file use **java -jar application.jar --file path/to/test-file.txt**.

```
cd app-text-operations-java
mvn clean install
cd target
java -jar application.jar --file path/to/test-file.txt
```

## Options of operations



**| operation | switch for cmd |        description        |**

| unique            |       -u       | filters unique lines only |

| sort      |       -s       | sorts lines naturally     |

| duplicates|       -d       | filters duplicates only   |

| lines      |       lines       | prints all lines (default opeartion)     |

| count      |       count       | counts lines     |

| similar      |       similar       | pairs two most similar (can't be exactly two same lines) and counts Levenshtein distance  |



## Changing operation
```
java -jar application.jar --file path/to/test-file.txt -u 
java -jar application.jar --file path/to/test-file.txt -s
java -jar application.jar --file path/to/test-file.txt count
java -jar application.jar --file path/to/test-file.txt -u count 
```

